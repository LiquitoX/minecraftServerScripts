#!/bin/bash

# Set global variables. Change main_path, backup_path, log_path and servers to reflect
# your envoirment. Please note that these paths need to be absolute!
main_path="/var/Minecraft"
backup_path="/var/Minecraft/Backups"
log_path="/var/Minecraft/Logs"
build_path="/var/Minecraft/Builds"

# Age in days that the backups should be retained. (-1 will remove everything)
backup_age=-1

# Servers ( Array should be in the following format:
# servers=("name1" "name2") )
servers=("server_name_here")

# Set all argument variables.
arg=${1}
version=${2} # Can also be server_name when removing a server.
server_name=${3}
server_port=${4}
min_mem=${5}
max_mem=${6}
eula=${7}

clear

# Function process_initial_input
#
# This function processes all initial inputs or if there
# is none, it will show a help menu.
function process_initial_input(){
  # Process the given input.
  if [[ -z $arg || $arg = "-h" ]]; then
    show_help
    exit
  elif [[ $arg = "-i" ]]; then
    echo "Installing dependencies!"
    install_dependencies
  elif [[ $arg = "-s" ]]; then
    echo "Starting servers!"
    start_servers
  elif [[ $arg = "-r" ]]; then
    echo "Rebooting servers!"
    restart_servers
  elif [[ $arg = "-b" ]]; then
	echo "Backing up servers!"
	for server in "${servers[@]}"; do
		( ( backup_server $server & ) & )
  	done
  elif [[ $arg = "-rm" ]]; then
    echo "Removing backups!"
    remove_backups
  elif [[ $arg = "-n" ]]; then
    new_server $version $server_name $server_port $min_mem $max_mem $eula
  elif [[ $arg = "-bn" ]]; then
    echo "Building new server jar!"
    build_server $version
	elif [[ $arg = "-rms" ]]; then
    echo "Removing server!"
    remove_server $version
	elif [[ $arg = "-rs" ]]; then
    echo "Server restore operation started!"
    restore_server $version
  else
    echo "Argument is invalid!"
  fi
}


# Function show_help
#
# This function will print out all possible commands
# that this file can handle.
function show_help(){
  clear
  cat <<- EOF
	Help and usage:
	Usage: main.sh <arguments> [optional args]
	-h          Shows this menu.
	-i          Install dependencies.
	-s          Starts the servers.
	-r          Restarts the servers.
	-b          Makes a backup of the servers.
	-rm         Removes the server backups.
	-n <args>   Creates and builds a new server.
	-bn <ver>   Builds a new server Jar for updating spigot instances.
	-rs <name>  Restore a server.
	-rms <name> Removes the specified server.

	Detailed usage:
	Creating a new server:
	main.sh -n version server_name server_port min_mem max_mem accept_eula
	Example:
	main.sh -n 1.16.2 hub 25577 512 512 true

	Creating a new server build:
	main.sh -bn version
	Example:
	main.sh -bn 1.16.2

	Removing a server:
	main.sh -rms name
	Example:
	main.sh -rms hub
	EOF
}


# Function create_screen
#
# This function will create a screen when needed.
function create_screen {
  # Get screenName first
  screen_name=$1
  # Create said screen
  screen -S "$screen_name" -d -m
}


# Function send_screen_command
#
# This function will send a command to the specified screen.
function send_screen_command(){
  # Get the screenname.
  screen_name=$1
  # Command to be executed.
  command=$2
  # Execute the above command and press "enter".
  screen -S $screen_name -X stuff "$command\n"
}


# Function install_dependencies
#
# This function will install all needed dependencies to run the servers with this script.
function install_dependencies(){
  echo "The following packages will be installed: openjdk-19-jre screen"
  read -p "Are you sure? This will need root privileges. (y/N) " -n 1 -r
  echo
  if [[ $REPLY =~ ^[Yy]$ ]]; then
      sudo apt update
      sudo apt install -y screen openjdk-19-jre
      echo "Everything is installed!"
  else
    echo "There is nothing more I can do. Exiting..."
    exit
  fi
	# Create the directories specified by the user up top.
	cat <<- EOF
    Please make sure you have set the variables and paths before you continue!
    We will now create the following folders:
    $main_path
    $backup_path
    $build_path
    $log_path
    ${main_path}/Servers
	EOF
	read -p "This will require root privileges, but set the ownership to the current user. Continue? (y/N) " -n 1 -r

	if [[ $REPLY =~ ^[Yy]$ ]]; then
    echo "Creating directories."
    sudo mkdir $main_path
    sudo mkdir $backup_path
    sudo mkdir $build_path
    sudo mkdir $log_path
    sudo mkdir ${main_path}/Servers
    echo "Setting ownership to the current user."
    sudo chown $USER:$USER $main_path
    sudo chown $USER:$USER $backup_path
    sudo chown $USER:$USER $build_path
    sudo chown $USER:$USER $log_path
    sudo chown $USER:$USER ${main_path}/Servers
    echo "Done! Please re-run this script with another option to install a new server!"
    exit
  else
    echo "Installation failed! Please create the folders yourself!"
	exit
  fi
}


# Function start_servers
#
# This function will start all servers that are defined.
function start_servers(){
  # Iterate over all servers.
  for server in "${servers[@]}"; do
    # Set the server path.
    server_path=$main_path/Servers/$server/
    # Check if the server path/directory exists.
    if [ ! -d "$server_path" ]; then
      echo "Directory/Server does not exist. Please check your variables!"
      exit 1
    else
      # Start the screen for the server.
      create_screen $server
      # Move into the directory in the screen.
      send_screen_command $server "cd $server_path"
      # Start the server with boot.sh.
      send_screen_command $server "./boot.sh"
    fi
  done
}


# Function restart_servers
#
# This function will restart all servers that are defined.
function restart_servers(){
  # Iterate over all servers.
  for server in "${servers[@]}"; do
    send_screen_command $server "kickall Server restarting..."
    send_screen_command $server "stop"
  done
}


# Function backup_servers
#
# This function will backup all servers that are defined.
function backup_server(){
	server=$1
	# Set the server directory
	server_path=$main_path/Servers/$server
	# Check if the server/Directory exists.
	if [ ! -d "$server_path" ]; then
	  echo "Server not found! I will now exit."
	  exit
	fi
	# Check if the backup directory exists.
	if [ ! -d "$backup_path" ]; then
		echo "Backup directory does not exist, please check your definitions!"
		exit
	fi
	# Set the filename for the backup.
	file_name="$(date '+%y-%m-%d_%H')00_"$server".tar.gz"
	# Notify everyone on the server for the backup.
	send_screen_command $server "say Server backup will begin shortly. Please beware of lag that can occur."
	# Save the world.
	send_screen_command $server "save-off"
	send_screen_command $server "save-all"
	# Notify everyone that the backup has started.
	send_screen_command $server "say Started backup on $(date '+%d-%m-%y_%H:%M:%S')"
	# Start the actual backup.
	tar -czf "$backup_path/$file_name" -C ${server_path} .

	# https://unix.stackexchange.com/questions/427669/compress-using-tar-without-saving-full-path?noredirect=1&lq=1

	# tar --exclude="$server_path/plugins/dynmap/web" -czf "$backup_path/$file_name" "$server_path"
	# calculate the size of the backup.
	file_size=$(stat -c%s "$backup_path/$file_name")
	file_size=$(expr $file_size / 1024 / 1024)
	# Notify everyone that the backup has completed, with the filesize.
	send_screen_command $server "say Backup completed on $(date '+%d-%m-%y_%H:%M:%S')"
	send_screen_command $server "say Size of the backup is $file_size MB"
	# Turn on autosave of the server again.
	send_screen_command $server "save-on"
}


# Function remove_backups
#
# This function will remove all backups that are older than the specified amount.
# This function assumes that you have a backup running at least once every day
# around 00:00 and 00:59 so that the filename still includes name_0000.tar
function remove_backups(){
  # Remove everything except the backups made between 00:00 and 00:59.
  find $backup_path/* | grep -v 0000 | xargs rm -rv

  # Now we remove everything older than the defined value.
  find $backup_path/* -mtime +$backup_age -exec rm -v {} \;
}


# Function new_server
#
# This function will create a new server with the given arguments. If the arguments are not given, it will ask for these arguments.
function new_server(){
  # Check if all needed arguments are set. If not, ask them one by one for the user input.
  if [ -z "$version" ] || \
  [ -z "$server_name" ] || \
  [ -z "$server_port" ] || \
  [ -z "$min_mem" ] || \
  [ -z "$max_mem" ] || \
  [ -z "$eula" ]; then
    echo "Not all arguments were found, please specify them below."
    echo "Please specify the version: (e.g. 1.16.4)"
    read version
    echo "Thanks, now the server name: (e.g. factions)"
    read server_name
    echo "Now the port that you want the server to run on: (e.g. 25565)"
    read server_port
    echo "Please enter the minimum amount of RAM the server can use in M: (e.g. 512)"
		read min_mem
		echo "Please enter the maximum amount of ram the server can use in M: (e.g. 1024)"
		read max_mem
		echo "Do you accept the EULA of Mojang for the server software? (true/false)"
		read eula
  fi

	# Return the values to the user to let them check if it is correct, if not, restart the server creation.
	cat <<- EOF
    The following arguments were given:
    Server Version: $version
    Server Name:    $server_name
    Server Port:    $server_port
    Minimum memory: $min_mem
    Maximum memory: $max_mem
    EULA Accepted:  $eula
	EOF

	read -p "Are these values correct? (y/N) " -n 1 -r
  echo
  if [[ ! $REPLY =~ ^[Yy]$ ]]; then
			eula=""
			clear
			echo "Arguments have been reset!"
			new_server
  fi

	# Let us trust the user and continue with the creation of the server.

	# Check if a directory for this server already exists. If not, create it.
  server_path=$main_path"/Servers/"$server_name
  if [ -d "$server_path" ]; then
    echo "This server name already exists! Please try again."
    exit
  else
    echo "Creating server folder."
    mkdir $server_path
  fi

	# Move over the boot file with EOF
  echo "Deploying boot.sh for the server: $server_name"
  cat <<- EOF > $server_path/boot.sh
  #!/bin/bash
  # This is the default boot.sh script generated by insert_git_repo.
  function run_server(){
    java -Xmx${max_mem}M -Xms${min_mem}M -jar server.jar -nogui
    echo "Press ^C to exit. Server will restart in 5 seconds!"
    sleep 5
    run_server
  }
  run_server
	EOF

	# Make the boot.sh file executable.
	echo "Making $server_name/boot.sh executable."
	chmod +x $server_path/boot.sh

	# Check if the EULA has been accepted, if not, ask the user to do it now.
	if [ ! "$eula" = "true" ]; then
		echo "You really need to accept the EULA now, please enter true (true/false):"
		read eula
		# Check if the user REALLY has accepted the eula, if not, exit.
		if [ ! "$eula" = "true" ]; then
	  	rm -rf $server_path
			echo "You still have not accepted the EULA, this script will now exit. Please try again!"
			exit
		fi
	fi
	# Deploy eula.txt
	echo "eula=true" > $server_path/eula.txt

	# Ask the user if they would like to setup server.properties and deploy this.
	read -p "Do you also want to deploy a defined server.properties file? (y/N) " -n 1 -r
  echo
  if [[ $REPLY =~ ^[Yy]$ ]]; then
		# Start asking the questions for server.properties.
		echo "What gamemode should the server be? (creative/survival/adventure/spectator)"
		read gamemode
		echo "What difficulty should the server be? (easy/normal/hard)"
		read difficulty
		echo "What should the world be called? ($server_name)"
		read world_name
		echo "What is the maximum amount of players that you would like on the server? (0-100)"
		read max_players
		echo "Should command blocks be enabled? (true/false)"
		read enable_command_block
		echo "Please enter the message of the day for the server:"
		read motd
		clear
		cat <<- EOF
		The following arguments were specified:
		Gamemode:                $gamemode
		Difficulty:              $difficulty
		World Name:              $world_name
		Max Players:             $max_players
		Command Blocks enabled:  $enable_command_block
		Message of the Day:      $motd
		EOF
		read -p "Is this correct? (y/N) " -n 1 -r
		echo
		# Check if this is correct, if not remove the server and make the user start over.
		if [[ ! $REPLY =~ ^[Yy]$ ]]; then
			echo "Please create the server again."
			rm -rf $server_path
			exit
		fi

		# Deploy te actual server.properties file.
		echo "Deploying defined server.properties."
		cat <<- EOF > $server_path/server.properties
		# Minecraft server properties
		#
		server-port=$server_port
		gamemode=$gamemode
		difficulty=$difficulty
		level-name=$world_name
		max-players=$max_players
		enable-command-block=$enable_command_block
		motd=$motd
		EOF

	else 
		# The user does not want to deploy a more advanced server.properties.
		echo "Deploying basic server.properties."
		cat <<- EOF > $server_path/server.properties
		# Minecraft server properties
		#
		server-port=$server_port
		EOF
  fi

	# Start building the $version and move the server.jar over.
	build_server $version

	# Copy the newly made server.jar into $server_path
	cp ${build_path}/${version}/server.jar ${server_path}/server.jar

	# Ask the user to change the config file to include the newly made server.
	echo "Server creation finished! Please add \"$server_name\" to your servers in your configuration at the top!"
	exit
}


# Function build_server
#
# This function will download the newest version of the Spigot BuildTools. When that is done, the server wil be built.
function build_server(){
	# Check if the version is set
	if [ -z "$version" ]; then
		echo "Please enter the version you want to build: (e.g. 1.16.4)"
		read version
	fi

	# Remove the old buildtools if it exists.
	if [ -f "${build_path}/buildTools.jar" ]; then
		echo "Removing old buildTools.jar!"
		rm ${build_path}/buildTools.jar
	else
		echo "buildTools not found, proceeding..."
	fi

	# Download the latest buildTools.
	echo "Downloading latest version of buildtools!"
	wget -q -O "${build_path}/buildTools.jar" https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar

	# Check if the version already exists. if yes, remove it.
	if [ -d "${build_path}/${version}" ]; then
		echo "Version already exists, removing!"
		rm -rf ${build_path}/${version}
	fi

	# Create the directory for the new build.
	echo "Creating new build directory!"
	mkdir ${build_path}/${version}

	# Copy the newly downloaded buildtools over.
	cp ${build_path}/buildTools.jar ${build_path}/${version}/buildTools.jar

	# Set old path and move into the new directory.
	current_path=`pwd`
	cd ${build_path}/${version}

	# Run the buildtools.
	echo "Building Spigot Server $version. This can take a few minutes..."
	java -jar -Xmx1G ./buildTools.jar --rev $version > ${build_path}/latest.log

	# Move the server jar out of the way.
	mv ./spigot-$version.jar ../server.jar

	# Remove everything in this folder.
	rm -rf ./*

	# Move server jar back in.
	mv ../server.jar ./

	# Done.
	echo "Server build done, you can find the server.jar in ${build_path}/${version}! If there was an error, please remove the directory at $server_path and try again!"
}


# Function remove_server
#
# This function let's you select a server which you can then remove.
# TODO: Implement switch so you can run main.sh -rms server_name without any questions.
function remove_server {
	# Move into the server directory
	cd ${main_path}/Servers

	# Check is $version is set, if yes, rename it.
	if [ ! -z "${version}" ]; then
		server_name=$version
		# Check if the server exists.
		if [ -d "$server_name" ]; then 
			rm -rf ${main_path}/Servers/${server_name}
			echo "Server removed! If you have made backups, these still exist."
			exit
		else
			# Server does not exist.
			echo "Wrong server or server does not exist. Please try again!"
			exit
		fi
	else
		# Name is not set, ask which server the user wants to remove.
		cat <<- EOF
		Type the number of the server you want to remove, or type 'stop' to exit this program.
		EOF
		
		# Show all servers in this directory
		select name in * "stop"; do
			# Leave if the user says 'stop'.
			if [ "$REPLY" == stop ]; then exit; fi
			if [ -d "$name" ]; then
				echo "${name} selected!"
				read -p "Do you really want to remove this server? (y/N) " -n 1 -r
				echo
				if [[ $REPLY =~ ^[Yy]$ ]]; then
					echo "Removing server, this can take a few moments..."
					rm -rf ${main_path}/Servers/${name}
					echo "Server removed! If you have made backups, these still exist."
					exit
				else
					echo "Please run this script again to remove the correct server!"
				fi
				exit
			else
				echo "Incorrect number or no servers were found!"
				exit
			fi
		done
	fi
}


# Function restore_server
#
# This function will show you the available backups from the specified server.
function restore_server(){
	# Check if $version is set (Which is in this case the server name.)
	if [ ! -z "$version" ]; then
		# Rename version to server_name for easier reading.
		server_name=$version
		server_path=${main_path}/Servers/${server_name}

		# Create the array with all backups that contain the server_name.
		unset options i
		while IFS= read -r -d $'\0' f; do
  		options[i++]="$f"
		done < <(find $backup_path -maxdepth 1 -type f -name "*"$server_name".tar.gz" -print0 )

		# Create a menu so the user can select the backup to restore.
		select opt in "${options[@]}" "Exit"; do
			case $opt in
				*.tar.gz)
					echo "Backup $opt chosen!"
					# Set file_name for easier reading.
					file_name=$opt
					echo $file_name

					# Ask if the user wants to restore the backup to a new folder.
					echo "Please enter the name to which this backup should restore: ($server_name) [Please note: If the name given already exists, it will be removed, this cannot be undone!]"
					read server_name

					# Check if that folder already exists and remove it.
					if [[ -d ${main_path}/Servers/${server_name} ]]; then
						echo "Old server has been removed!"
						rm -rf ${main_path}/Servers/${server_name}
					fi

					# Create a new folder for the restore.
					echo "Restoring the server..."
					mkdir ${main_path}/Servers/${server_name}
					tar -xzf $file_name -C ${main_path}/Servers/${server_name} .
          echo "Server has been restored! Please do not forget to edit/add the restored server to your servers up top!"
					
					break
					;;
				"Exit")
					echo "Exiting."
					exit
					break
					;;
				*)
					echo "This is not a valid option."
					;;
			esac
		done
		
	else
		echo "Server name is empty, please try again with main.sh -rs <name>!"
	fi
}


# Run the script.
process_initial_input
